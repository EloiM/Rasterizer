#include <stdlib.h>
#include <stdio.h>
#include <math.h>
#include <time.h>
#include <SDL.h>
#include "graphics.h"
#include "3Dengine.h"


void wait()
{
    SDL_Event event = {0};

    SDL_Flip(SDL_GetVideoSurface());
    while(event.type != SDL_QUIT && event.key.keysym.sym != SDLK_ESCAPE)
        SDL_WaitEvent(&event);
}

void init()
{
    SDL_Surface *screen;

    if(SDL_Init(SDL_INIT_VIDEO) < 0)
        exit(0);

    atexit(SDL_Quit);

    screen = SDL_SetVideoMode(_ScreenW,_ScreenH,32,SDL_SWSURFACE);
    if(screen == NULL)
        exit(0);

    srand(time(NULL));

}

int main(int argc,char *argv[])
{
        /*** VARS ***/
    Point3D shape3D[6][4] = {
                                 { {600,600,3000}, {600,1200,3000}, {1200,1200,3000},{1200,600,3000} }, //le fond
                                 { {600,1200,2400},{600,1200,3000}, {1200,1200,3000},{1200,1200,2400} }, //le bas
                                 { {1200,600,2400},{1200,1200,2400},{1200,1200,3000},{1200,600,3000} }, //la face droite
                                 { {600,600,3000}, {600,1200,3000}, {600,1200,2400}, {600,600,2400} }, //la face gauche
                                 { {600,600,3000}, {600,600,2400},  {1200,600,2400}, {1200,600,3000} }, //le haut
                                 { {600,600,2400}, {600,1200,2400}, {1200,1200,2400},{1200,600,2400} }, //l'avant
                            };
    Point3D vector1 = {-900,-900,-2500}, vector2 = {900,900,2500};
    Point2D shape2D[6][4];
    Uint32 color[6] = {0xff0000, 0xaaaa00, 0x777777, 0x00aaaa, 0x0000ff};
    SDL_Event event;
    float a = 0, b = 0, c = 0;
    int i, j;

        /*** SETUPS ***/
    init();
    SDL_Surface *screen = SDL_GetVideoSurface();
    if(!setVisionComplex(130, 4.0/3,(_ScreenW/2)/tan((M_PI*(60.0/180))/2), 2000))
        printf("Error on Vision complex loading\n");

    SDL_EnableKeyRepeat(1,1);

        /*** MAIN ***/
    while(1)
    {
        SDL_WaitEvent(&event);

        switch(event.type)
        {
            case SDL_KEYDOWN:
                switch(event.key.keysym.sym)
                {
                    case SDLK_LEFT:     a=M_PI/100;   break;
                    case SDLK_RIGHT:    a=-M_PI/100;  break;
                    case SDLK_UP:       b=M_PI/100;   break;
                    case SDLK_DOWN:     b=-M_PI/100;  break;
                    case SDLK_b:        c=M_PI/100;   break;
                    case SDLK_n:        c=-M_PI/100;  break;
                    case SDLK_ESCAPE:   goto end;
                    default:                            break;
                }
                break;
            case SDL_KEYUP:
                a = 0;
                b = 0;
                break;
        }

        SDL_FillRect(screen, NULL, 0xffffff);

        for(j = 0; j < 6; j++)
            for(i = 0; i < 4; i++)
            {
                translate(vector1, &shape3D[j][i]);

                rotation_z(c, &shape3D[j][i]);
                rotation_y(a, &shape3D[j][i]);
                rotation_x(b, &shape3D[j][i]);

                translate(vector2, &shape3D[j][i]);

                if(!rasterize_point(shape3D[j][i], &shape2D[j][i]))
                    printf("Point %d is out of frustrum\n", i);
            }


        /* affichage */
        for(i = 0; i < 4; i++){
            line(screen, 0, shape2D[0][i].x, shape2D[0][i].y, shape2D[0][(i+1)%4].x, shape2D[0][(i+1)%4].y);
            line(screen, 0, shape2D[5][i].x, shape2D[5][i].y, shape2D[5][(i+1)%4].x, shape2D[5][(i+1)%4].y);
            line(screen, 0, shape2D[0][i].x, shape2D[0][i].y, shape2D[5][i].x, shape2D[5][i].y);
        }

        SDL_Flip(screen);
        SDL_Delay(10);
    }
    end:

        /*** QUIT ***/
    SDL_SaveBMP(screen, "out.bmp");
    return EXIT_SUCCESS;
}
