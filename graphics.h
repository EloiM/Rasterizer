#ifndef GRAPHICS_H_INCLUDED
#define GRAPHICS_H_INCLUDED

#include <SDL.h>
#include "structs.h"

void point(SDL_Surface*, int, int, Uint32);
void line(SDL_Surface*, Uint32, int, int, int, int);
void SDL_PutPixel(SDL_Surface*, int, int, Uint32);
void turn(Point2D*, int);
int max(Point2D*, int); //Place la valeur la plus basse en premi�re. Conserve l'ordre.
void scanline(SDL_Surface*, Uint32, Point2D*, int);
SDL_Surface* horizontal_mirror(SDL_Surface*); //Immpos� par le rep�re invers� de la SDL

#endif // GRAPHICS_H_INCLUDED
