#include <limits.h>
#include <math.h>
#include "graphics.h"

void SDL_PutPixel(SDL_Surface *sur, int x, int y, Uint32 pixel)
{
    int bpp = sur->format->BytesPerPixel;

    if (x<0 || x>=sur->w|| y<0 || y>=sur->h)   // securit� : si un pixel est dehors : on ne l'�crit pas.
        return;

    unsigned char *p = (unsigned char *)sur->pixels + y * sur->pitch + x * bpp;
    if (bpp==4)
        *(Uint32*)p = pixel;
}

void scanline(SDL_Surface *sur, Uint32 color, Point2D* edges, int num_edges)
{
    float scan_from, scan_to; //l'abcisse de scan des deux cot�s
    int i_to = num_edges-1, i_from = 1; //l'indice de scan
    float coef_from, coef_to;
    int y;
    int min_y, max_y;
    SDL_Rect pos;

    turn(edges, num_edges);
    min_y = edges[0].y;
    max_y = max(edges, num_edges);

    scan_from = edges[0].x;
    scan_to = edges[0].x;
    coef_from = (double)(edges[1].x - edges[0].x)/(edges[1].y - edges[0].y);
    coef_to = (double)(edges[num_edges-1].x - edges[0].x)/(edges[num_edges-1].y - edges[0].y);

    pos.h = 1;

    for(y = min_y; y <= max_y; y++)
    {
        if(y >= edges[i_from].y){
            scan_from = edges[i_from].x;
            coef_from = (double)(edges[i_from+1].x - edges[i_from].x)/(edges[i_from+1].y - edges[i_from].y);
            i_from++;
        }
        if(y >= edges[i_to].y){
            scan_to = edges[i_to].x;
            coef_to = (double)(edges[i_to].x - edges[i_to-1].x)/(edges[i_to].y - edges[i_to-1].y);
            i_to--;
        }

        /* Les arrondis sont arbitraires mais donnent un rendu plus proche de la r�alit� */
        if(y > 0 && y < sur->h)
        {
            pos.x = (int)ceil((scan_from < scan_to) ? scan_from : scan_to);
            pos.w = abs(scan_from-floor(scan_to));
            pos.y = y;
            SDL_FillRect(sur, &pos, color);
        }


        scan_from += coef_from;
        scan_to += coef_to;
    }
}

int max(Point2D *tab, int size)
{
    int i, max = 0;

    for(i = 0; i < size; i++)
        if(tab[i].y > max)
            max = tab[i].y;

    return max;
}

void turn(Point2D *tab, int size)
{
    int i, j, min = INT_MAX;
    Point2D *tmp = (Point2D*)malloc(size * sizeof(Point2D));

    for(i = 0; i < size; i++)
    {
        tmp[i] = tab[i];//on copie

        if(tab[i].y < min){
            min = tab[i].y; //on stocke la valeur minimale actuelle
            j = i; //et son indice dans le tableau
        }
    }

    for(i = 0; i < size; i++)
        tab[i] = tmp[(i+j)%size];

    free(tmp);
}

void point(SDL_Surface *sur, int x, int y, Uint32 color)
{
    int i;

    for(i = 0; i <= 10; i++)
    {
        SDL_PutPixel(sur, x - 10/2 + i, y, color);
        SDL_PutPixel(sur, x, y - 10/2 + i, color);
    }
}


void line(SDL_Surface *sur, Uint32 color, int x1, int y1, int x2, int y2)
{
    int x = x1,y = y1; /* piqu� sur le site de fvirtman */
    int Dx,Dy;
    int xincr,yincr;
    int erreur;
    int i;

    Dx = abs(x2-x1);
    Dy = abs(y2-y1);
    if(x1<x2)   xincr = 1;
    else        xincr = -1;
    if(y1<y2)   yincr = 1;
    else        yincr = -1;

    SDL_LockSurface(sur);
    if(Dx>Dy)
    {
        erreur = Dx/2;
        for(i=0;i<Dx;i++)
        {
            x += xincr;
            erreur += Dy;
            if(erreur>Dx)
            {
                erreur -= Dx;
                y += yincr;
            }
        SDL_PutPixel(sur, x, y, color);
        }
    }
    else
    {
        erreur = Dy/2;
        for(i=0;i<Dy;i++)
        {
            y += yincr;
            erreur += Dx;
            if(erreur>Dy)
            {
                erreur -= Dy;
                x += xincr;
            }
        SDL_PutPixel(sur , x, y, color);
        }
    }
    SDL_UnlockSurface(sur);
}
