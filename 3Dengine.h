#ifndef ENGINE_H_INCLUDED
#define ENGINE_H_INCLUDED

#include "structs.h"

/* Initialise les variables globales se chargeant du
complexe de vision (cam�ra, near, far...) */
int setVisionComplex(int, float, float, float);

/* Le tableau de 3 points en 3D, qui sera rast�riz� et
stock� dans le tableau en 2D. La taille (ou nombre de triangle)
sera stock�e dansun int, car si il y a clipping, il y aura
plus de sommets. Retourne un int pour indiquer si le triangle
a besoins d'�tre rast�riz� (en dehors du frustrum) */
int rasterize_point(Point3D, Point2D*);

void camera_moveto(float, float, float);
/* void camera_lookat(float, float, float); */
/* void camera_upsetting(float, float, float); */

int rotation_x(float, Point3D*); /* Utilise des matrices */
int rotation_y(float, Point3D*);
int rotation_z(float, Point3D*);
int translate(Point3D, Point3D*);

float* multiply_matrix(float[4][4], float[4]); //Multiplie deux matrices, l'une de 4x4, l'autre de 4x1

#endif //ENGINE_H_INCLUDED
