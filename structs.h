#ifndef STRUCTS_H_INCLUDED
#define STRUCTS_H_INCLUDED

typedef struct{
    int x, y;
}Point2D;

typedef struct{
    float x, y, z; /* Largeur, hauteur, profondeur */
}Point3D;

typedef struct{
    /* Un plan rectangle vertical repr�sentant l'�cran ou le fond
    Ici, il sera foc�ment non rotatif et paral�le au plan (O,x,y) */
    Point3D pos; //point en haut a gauche
    float w, h; //Les dimensions
}Plane;

typedef struct{
    Point3D pos, at, up;
}Eye;

#define _ScreenW        800
#define _ScreenH        600

#endif // STRUCTS_H_INCLUDED
