#include <stdlib.h>
#include <stdio.h>
#include <math.h>
#include "3Dengine.h"
#include "structs.h"

Eye g_camera;
Plane g_near, g_far;
float g_min, g_max;

int setVisionComplex(int angle, float ratio, float min, float max)
{
    if(angle == 0 || angle >= 180 ||
       min >= max || ratio == 0 || min == 0)
        return 0;

    g_near.w = 2*min*tan((M_PI*((float)angle/180))/2); //on a une conversion degr�s-radians
    g_near.h = (1.0/ratio)*g_near.w;
    g_far.w = g_near.w*(max/min);
    g_far.h = g_near.h*(max/min);
    g_min = min;
    g_max = max;

    /* On met la position de la cam a (0,0,0) par d�faut*/
    g_camera.pos.x = 0;
    g_camera.pos.y = 0;
    g_camera.pos.z = 0;
    g_near.pos.x = -(g_near.w/2);
    g_near.pos.y = -(g_near.h/2);
    g_near.pos.z = g_min;
    g_far.pos.x = -(g_far.w/2);
    g_far.pos.y = -(g_far.h/2);
    g_far.pos.z = g_max;


    return 1;
}

void camera_moveto(float x, float y, float z)
{
    float xrel, yrel, zrel; //Le d�placement par rapport au point d'origine

    xrel = x-g_camera.pos.x;
    yrel = y-g_camera.pos.y;
    zrel = z-g_camera.pos.z;

    g_camera.pos.x = x;
    g_camera.pos.y = y;
    g_camera.pos.z = z;
    g_near.pos.x += xrel;
    g_near.pos.y += yrel;
    g_near.pos.z += zrel;
    g_far.pos.x += xrel;
    g_far.pos.y += yrel;
    g_far.pos.z += zrel;
}

/* la cam�ra peut bouger sur l'axe z (av-ar)
et sur le plan (O, x, y), conform�ment a la
int rasterize_point(Point3D p1, Point2D *p2)
{
    float ratio = (g_min+g_camera.pos.z)/p1.z;

    p2->x = (int)(ratio*p1.x - g_camera.pos.x);
    p2->y = (int)(ratio*p1.y + g_camera.pos.y);

    return 1;
}*/

int rasterize_point(Point3D p1, Point2D *p2)
{
    float ratio = (g_min+g_camera.pos.z)/p1.z;

    p2->x = (int)(ratio*(p1.x - g_camera.pos.x));
    p2->y = (int)(ratio*(p1.y - g_camera.pos.y));

    return 1;
}

int rotation_x(float angle_rad, Point3D *p)
{
    float point[4] = {p->x, p->y, p->z, 1}, *res;
    float matrix[4][4] = {{1, 0,              0,               0},
                          {0, cos(angle_rad), -sin(angle_rad), 0},
                          {0, sin(angle_rad), cos(angle_rad),  0},
                          {0, 0,              0,               1}};
    res = multiply_matrix(matrix, point);

    p->x = res[0]/res[3];
    p->y = res[1]/res[3];
    p->z = res[2]/res[3];
}

int rotation_y(float angle_rad, Point3D *p)
{
    float point[4] = {p->x, p->y, p->z, 1}, *res;
    float matrix[4][4] = {{cos(angle_rad),  0, sin(angle_rad), 0},
                          {0,               1, 0,              0},
                          {-sin(angle_rad), 0, cos(angle_rad), 0},
                          {0,               0, 0,              1}};
    res = multiply_matrix(matrix, point);

    p->x = res[0]/res[3];
    p->y = res[1]/res[3];
    p->z = res[2]/res[3];
}

int rotation_z(float angle_rad, Point3D *p)
{
    float point[4] = {p->x, p->y, p->z, 1}, *res;
    float matrix[4][4] = {{cos(angle_rad), -sin(angle_rad), 0, 0},
                          {sin(angle_rad), cos(angle_rad),  0, 0},
                          {0, 0,              1,               0},
                          {0, 0,              0,               1}};
    res = multiply_matrix(matrix, point);

    p->x = res[0]/res[3];
    p->y = res[1]/res[3];
    p->z = res[2]/res[3];
}

int translate(Point3D vector, Point3D *p)
{
    float point[4] = {p->x, p->y, p->z, 1}, *res;
    float matrix[4][4] = {{1, 0, 0, vector.x},
                          {0, 1, 0, vector.y},
                          {0, 0, 1, vector.z},
                          {0, 0, 0, 1}};
    res = multiply_matrix(matrix, point);

    p->x = res[0]/res[3];
    p->y = res[1]/res[3];
    p->z = res[2]/res[3];
}

float* multiply_matrix(float matrix[4][4], float vector[4])
{
    int i, j;
    float res[4] = {0,0,0,0};

    for(i = 0; i < 4; i++)
        for(j = 0; j < 4; j++)
            res[i] += matrix[i][j] * vector[j];

    return res;
}

